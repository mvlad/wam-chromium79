// Copyright (c) 2008-2018 LG Electronics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#include "WindowTypes.h"

const char WT_CARD[] = "_WEBOS_WINDOW_TYPE_CARD";
const char WT_POPUP[] = "_WEBOS_WINDOW_TYPE_POPUP";
const char WT_MINIMAL[] = "_WEBOS_WINDOW_TYPE_RESTRICTED";
const char WT_OVERLAY[] = "_WEBOS_WINDOW_TYPE_OVERLAY";
const char WT_NONE[] = "_WEBOS_WINDOW_TYPE_NONE";
const char WT_FLOATING[] = "_WEBOS_WINDOW_TYPE_FLOATING";
const char WT_UNKNOWN[] = "_WEBOS_WINDOW_TYPE_UNKNOWN";
const char WT_SYSTEM_UI[] = "_WEBOS_WINDOW_TYPE_SYSTEM_UI";

