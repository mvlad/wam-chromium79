// Copyright (c) 2008-2018 LG Electronics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// SPDX-License-Identifier: Apache-2.0

#ifndef _WINDOWTYPES_H_
#define _WINDOWTYPES_H_

extern const char WT_CARD[];
extern const char WT_POPUP[];
extern const char WT_MINIMAL[];
extern const char WT_OVERLAY[];
extern const char WT_NONE[];
extern const char WT_FLOATING[];
extern const char WT_UNKNOWN[];
extern const char WT_SYSTEM_UI[];

#endif // _WINDOWTYPES_H_
